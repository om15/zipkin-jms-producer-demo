package com.example.zipkinservice3;

import java.util.Date;

import javax.jms.Topic;
import javax.websocket.OnMessage;

import org.apache.activemq.command.ActiveMQTopic;
//import org.apache.log4j.Logger;
import org.aspectj.bridge.AbortException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
@RestController
@Import(JmsTracingConfiguration.class)
public class ZipkinService3Application  {
	@Autowired
	JmsTemplate jmsTemplate;

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	/*@Bean
	public AlwaysSampler alwaysSampler() {
		return new AlwaysSampler();
	}
*/
	//private static final Logger LOG = Logger.getLogger(ZipkinController.class.getName());

	@GetMapping(value = "/") 
	public void callBackend() {
		    jmsTemplate.convertAndSend("backend", new Date());
		  }


	@Bean
	public Topic topic() {
		return new ActiveMQTopic("backend");
	}
	
	public static void main(String[] args) {
		SpringApplication.run(ZipkinService3Application.class, args);
	}
}


class ZipkinController {

	@Autowired
	RestTemplate restTemplate;


	
	/*@GetMapping(value = "/zipkin3")
	public String zipkinService1() {
		//LOG.info("Inside zipkinService 3..");
		// throw new ArithmeticException();
		
		 * String response = (String)
		 * restTemplate.exchange("http://localhost:8084/zipkin4", HttpMethod.GET, null,
		 * new ParameterizedTypeReference<String>() { }).getBody();
		 

		jmsTemplate.convertAndSend("test_queue", new String("xyz"));
		return "Hi...";
	}*/
}